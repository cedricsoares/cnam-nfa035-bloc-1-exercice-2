package fr.cnam.foad.nfa035.fileutils.simpleaccess.media;

import java.io.IOException;
import java.io.InputStream;

/**
 * Gestion de l'application
 * @param <T>
 * @author Cedric Soares
 * @version 1.0 (octobre 2021)
 */
public abstract class AbstractImageFrameMedia<T> {
    private T channel;

    /**
     * Constructeur vide
     */
    protected AbstractImageFrameMedia() {
    }

    /**
     * Getter du channel
     * @return channel
     */
    public T getChannel() {
        return channel;
    }

    /**
     * Setter du channel
     * @param channel
     */
    public void setChannel(T channel) {
        this.channel = channel;
    }

    /**
     * Constructeur avec channel
     * @param channel
     */
    AbstractImageFrameMedia(T channel){
        this.channel = channel;
    }

    /**
     * Getter encodedImageOutput()
     * @return
     * @throws IOException
     */
    public abstract Object getEncodedImageOutput() throws IOException;

    /**
     * Getter encodedImageInput
     * @return
     * @throws IOException
     */
    public abstract InputStream getEncodedImageInput() throws IOException;
}
