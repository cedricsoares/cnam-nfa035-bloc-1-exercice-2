package fr.cnam.foad.nfa035.fileutils.simpleaccess.media;

import java.io.*;

/**
 *  Implémentation d'ImageFrameMedia avec un ByteArrayOutputStream
 * @author Cedric Soares
 * version 1.0 (octobre 2021)
 */

public class ImageByteArrayFrame extends AbstractImageFrameMedia<ByteArrayOutputStream> {

    /**
     * Constructeur
     * @param image
     */
    public ImageByteArrayFrame(ByteArrayOutputStream image) { super(image); }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputStream getEncodedImageOutput() throws FileNotFoundException {
        return getChannel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new ByteArrayInputStream(getChannel().toByteArray());
    }
}
