package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.media.AbstractImageFrameMedia;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implémentation de la classe de désérialisation en Base64
 * @author Cedric Soares
 * @version 1.0 (octobre 2021)
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    private OutputStream deserializationOutput;

    /**
     * Constructeur de la classe
     * @param deserializationOutput
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream deserializationOutput) {
        this.deserializationOutput = deserializationOutput;
    }

    /**
     * {@inheritDoc}
     * @return le stream de déserialisation
     */
    @Override
    public OutputStream getSourceOutputStream() {return deserializationOutput;}

    /**
     * {@inheritDoc}
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }
}
