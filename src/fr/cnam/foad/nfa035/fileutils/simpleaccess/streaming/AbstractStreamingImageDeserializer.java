package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

/**
 * Classe abstraite de déserialisation
 * @param <M>
 * @author Cédric Soares
 * @version 1.0 (octobre 2021)
 */
public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
    /**
     * Méthode de déserialisation
     * @param media
     * @throws IOException
     */
    @Override
    public final void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}

