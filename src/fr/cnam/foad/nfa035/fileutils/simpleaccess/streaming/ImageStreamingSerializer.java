package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.media.ImageByteArrayFrame;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface de sérialisation
 * @param <S>
 * @param <M>
 * @author Cedric Soares
 * @version 1.0 (octobre 2021)
 */
public interface ImageStreamingSerializer<S, M> {

    /**
     * Méthode de sérialisation
     * @param source
     * @param media
     * @throws IOException
     */
    void serialize(S source, M media) throws IOException;
    <K extends InputStream> K getSourceInputStream(S source) throws IOException;
    <T extends OutputStream> T getSerializingStream(M media) throws IOException;

}
