package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.media.AbstractImageFrameMedia;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

/**
 * Implémentation du sérialiseur pour un Streaming en Base64
 * @author Cédric Soares
 * @version 1.0 (octobre 2021)
 */
public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer<File, AbstractImageFrameMedia> {

    /**
     * {@inheritDoc}
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    /**
     * {@inheritDoc}
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64OutputStream((OutputStream) media.getEncodedImageOutput());
    }
}
