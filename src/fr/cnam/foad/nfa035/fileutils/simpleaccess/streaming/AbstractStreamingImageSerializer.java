package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

/**
 * Classe abstraite de sérialisation
 * @param <S>
 * @param <M>
 * @author Cedric Soares
 * @version 1.0 (octobre 2021)
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {

    /**
     * Classe abstraite pour sérialisation
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public final void serialize(S source, M media) throws IOException {
        getSourceInputStream(source).transferTo(getSerializingStream(media));
    }
}
